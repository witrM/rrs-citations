 	
var app = angular.module('myApp', [
  'ngRoute',
  'ui.bootstrap',
  'myApp.publicationsCtrl'
]);
	
        
        
        

app.directive('linearChart2', function($window){
   return{
      restrict:'EA',
      template:"<svg width='850' height='200'></svg>",
       link: function(scope, elem, attrs){
           var salesDataToPlot=scope[attrs.chartData];
           var padding = 20;
           var pathClass="path";
           var xScale, yScale, xAxisGen, yAxisGen, lineFun;

           var d3 = $window.d3;
           var rawSvg=elem.find('svg');
           var svg = d3.select(rawSvg[0]);

           function setChartParameters(){

               xScale = d3.scale.linear()
                   .domain([salesDataToPlot[0].hour, salesDataToPlot[salesDataToPlot.length-1].hour])
                   .range([padding + 5, rawSvg.attr("width") - padding]);

               yScale = d3.scale.linear()
                   .domain([0, d3.max(salesDataToPlot, function (d) {
                       return d.sales;
                   })])
                   .range([rawSvg.attr("height") - padding, 0]);

               xAxisGen = d3.svg.axis()
                   .scale(xScale)
                   .orient("bottom")
                   .ticks(salesDataToPlot.length - 1);

               yAxisGen = d3.svg.axis()
                   .scale(yScale)
                   .orient("left")
                   .ticks(5);

               lineFun = d3.svg.line()
                   .x(function (d) {
                       return xScale(d.hour);
                   })
                   .y(function (d) {
                       return yScale(d.sales);
                   })
                   .interpolate("basis");
           }
         
         function drawLineChart() {

               setChartParameters();

               svg.append("svg:g")
                   .attr("class", "x axis")
                   .attr("transform", "translate(0,180)")
                   .call(xAxisGen);

               svg.append("svg:g")
                   .attr("class", "y axis")
                   .attr("transform", "translate(20,0)")
                   .call(yAxisGen);

               svg.append("svg:path")
                   .attr({
                       d: lineFun(salesDataToPlot),
                       "stroke": "blue",
                       "stroke-width": 2,
                       "fill": "none",
                       "class": pathClass
                   });
           }

           drawLineChart();
       }
   };
});       



app.directive('linearChart', function($window,$http){
   return{
      restrict:'EA',
      template:"<svg width='850' height='200'></svg>",
       link: function(scope, elem, attrs){
           //var publications=scope[attrs.chartData];
           var padding = 20;
           var pathClass="path";
           var xScale, yScale, xAxisGen, yAxisGen, lineFun;
                $http.get('/rrs_citations/web/app_dev.php/api/v1/publications/AVSK3bYAtut5FjxJ_M6q').
                        success(function (data) {
                            publications = data.result.hits.hits;
                            
//                        });

        console.log('publikace...');
        console.log(publications);
        
        function getAuthorsStr(authors) {
            var len, i;
            var str = '';
            for (i = 0, len = authors.length; i < len; ++i) {
                str += authors[i].full_name+ '; ';
                if(i > 3){
                    break;
                }
            }
            return str;
        }        
        
        var links = [];
        var nodes = [];
        var index, indexNodes, len, i, j, len2, len3;
        for (index = 0, len = publications.length; index < len; ++index) {            
            var exist = false;
            for (i = 0, len2 = nodes.length; i < len2; ++i) {
                if(nodes[i].id === publications[index]._id){
                    console.log('existuje');
                    exist = true;
                }
            }
            if(exist == false){
                var pub = {
                    'title':publications[index]._source.title,
                    'authors':getAuthorsStr(publications[index]._source.authors),
                    'id':publications[index]._id,
                    "type": "circle",
                    "size": 20,
                    "color": "#66CC33"
                    };
                nodes.push(pub);
            }
            console.log('reference');
            console.log(publications[index]._source.references.length);
            console.log(publications[index]._source.references);
            for (j = 0, len2 = publications[index]._source.references.length; j < len2; ++j) {
                console.log(i);
                console.log(publications[index]._source.references[j]);
                var exist = false;
                for (i = 0, len3 = nodes.length; i < len3; ++i) {
                    if('referenced_pub_id' in publications[index]._source.references[j] && 
                            nodes[i].id === publications[index]._source.references[j].referenced_pub_id){
                        console.log('existuje!!!!');
                        console.log(nodes[i].id);
                        console.log(publications[index]._source.references[j].referenced_pub_id);
                        exist = true;
                    }
                }                
                if(exist == false){
                    var pub = {
                        'title':publications[index]._source.references[j].title,
                        'authors':getAuthorsStr(publications[index]._source.references[j]),
                        'id': ('referenced_pub_id' in publications[index]._source.references[j])? publications[index]._source.references[j].referenced_pub_id: '',
                        "type": "circle",
                        "size": 20,
                        "color": "#66CC33"
                        };
                    nodes.push(pub);
                }            
            }
            
        }
        
        function getIndexById(nodes, id, title) {
            var len, i;
            console.log('get index id');
            console.log(id);
            console.log(title);
            for (i = 0, len = nodes.length; i < len; ++i) {
                if(id != 'undefined' && nodes[i].id === id){
                    console.log('MATCH!!');
                    console.log(id);
                    console.log(nodes[i]);
                    return i;
                }else if(nodes[i].title === title){
                    return i;
                }
            }
            return null;
        }
        
        // assign to links
        for (index = 0, len = publications.length; index < len; ++index) {        
            console.log('assign links begin');
            var pubNodesIndex = getIndexById(nodes, publications[index]._id);
            console.log('assign links refs');
            for (j = 0, len2 = publications[index]._source.references.length; j < len2; ++j) {
                    var refNodesIndex = getIndexById(nodes, publications[index]._source.references[j].referenced_pub_id, publications[index]._source.references[j].title);
                    var pub = {
                            "source": pubNodesIndex,
                            "target": refNodesIndex
                        };
                    links.push(pub);
                
            }
        }        
        
        console.log('DONE');
        console.log('nodes');
        console.log(nodes.length);
        console.log(nodes);
        console.log('links');
        console.log(links.length);
        console.log(links);        
        console.log('#####');
        
        var width = window.innerWidth;
        var height = window.innerHeight;
        var currentWidth = width;
        var currentHeight = height;
        var link_distance = 150;
        var charge = -50;
        var default_link_color = "#ccc";
        var default_arrow_color = "#888";
        var default_highlight_color = "#888";
        var default_arrow_opacity = 1;
        var highlight_color = "orange";
        var highlight_opacity = 0;
        var nominal_text_size = 10;
        var max_text_size = 24;
        var link_stroke = 1;
        var max_link_stroke = 4.5;
        var min_zoom = 0.1;
        var max_zoom = 7;
        var text_center = false;
        var d3 = $window.d3;
        var force = d3.layout.force()
            .linkDistance(link_distance)
            //.linkStrength()
            .charge(charge)
            .gravity(0)
            .size([currentWidth, currentHeight]);
        
        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .html(function (d) {
                return "<strong>Nazev:</strong> <span style='color:orange'>" + d.title + "</span>" +
                    "<br><strong>Autori:</strong> <span style='color:orange'>" + d.authors + "</span>";
            })
            .offset([-10, 0]);
    
           
           var rawSvg=elem.find('svg');
           var svg = d3.select(rawSvg[0]).call(tip);
            console.log(tip);
                var graph = { 
                  "links": links ,
                  "nodes": nodes
                }

            //d3.select("body").select("svg").remove();
            //var svg = d3.select("body").append("svg").call(tip);
            var zoom = d3.behavior.zoom().scaleExtent([min_zoom, max_zoom]);
            var g = svg.append("g");
            svg.style("cursor", "move");
                console.log(graph.links);
                console.log(graph.nodes);
            var linkedByIndex = {};
            graph.links.forEach(function (d) {
                linkedByIndex[d.source + "," + d.target] = true;
            });

            function isConnected(a, b) {
                return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index] || a.index == b.index;
            }

            force.nodes(graph.nodes)
                .links(graph.links)
                .start();

            var link = g.selectAll(".link")
                .data(graph.links)
                .enter().append("line")
                .attr("class", "link")
                .style("stroke-width", link_stroke)
                .style("stroke", default_link_color);

            var arrow = g.selectAll("path")
                .data(graph.links)
                .enter().append("svg:path")
                .attr("class", "link")
                .style("fill", default_arrow_color)
                .style("stroke-width", link_stroke)
                .style("stroke", default_arrow_color)
                .style("opacity", default_arrow_opacity);

            var node = g.selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .call(force.drag);

            var circle = node.append("circle")
                .attr("r", function (d) {
                    return d.size;
                })
                .style("fill", function (d) {
                    return d.color;
                })
                .style("stroke-width", link_stroke)
                .style("stroke", default_highlight_color)
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);

            var text = g.selectAll(".text")
                .data(graph.nodes)
                .enter().append("text")
                .attr("dy", ".35em")
                .style("font-size", nominal_text_size + "px");

            if (text_center) {
                text.text(function (d) {
                    return d.title;
                })
                    .style("text-anchor", "middle");
            } else {
                text.attr("dx", function (d) {
                    return d.size;
                })
                    .text(function (d) {
                        return '\u2002' + d.title;
                    });
            }

            var isNodeSelected = false, isNodeHighlighted = false;

            node.on("mouseover", mouseover)
                .on("mousedown", mousedown)
                .on("mouseout", mouseout);

            d3.select(window).on("mouseup", mouseup);

            function mouseover(d) {
                isNodeHighlighted = true;
                if (!isNodeSelected && isNodeHighlighted) {
                    highlightNodeWithRelations(d);
                }
            }

            function mouseout() {
                isNodeHighlighted = false;
                // d3.event.stopPropagation();
                if (!isNodeSelected) {
                    setDefaultStyles();
                }
            }

            function mousedown(d) {
        //            force.stop();
                if (isNodeSelected) {
                    setDefaultStyles();
                    isNodeSelected = false;
                } else {
                    highlightNodeWithRelations(d);
                    isNodeSelected = true;
                }
            }

            function mouseup() {
                force.stop();
            }

            function highlightNodeWithRelations(d) {
                circle
                    .style("opacity", function (o) {
                        return isConnected(d, o) ? 1 : highlight_opacity;
                    })
                    .style("stroke", function (o) {
                        return isConnected(d, o) ? highlight_color : default_highlight_color;
                    });

                text.style("opacity", function (o) {
                    return isConnected(d, o) ? 1 : highlight_opacity;
                }).html(function (o) {
                    return o.title;
                }).style("font-weight", function (o) {
                    return isConnected(d, o) ? "bold" : "normal";
                });

                var setHighlightColor = function (o) {
                    return o.source.index == d.index || o.target.index == d.index ? highlight_color : default_highlight_color;
                };

                link.style("opacity", function (o) {
                    return o.source.index == d.index || o.target.index == d.index ? 1 : highlight_opacity;
                }).style("stroke", setHighlightColor);

                arrow.style("stroke", setHighlightColor)
                    .style("fill", setHighlightColor)
                    .style("opacity", function (o) {
                        return o.source.index == d.index || o.target.index == d.index ? 1 : highlight_opacity;
                    });
            }

            function setDefaultStyles() {
                circle
                    .style("opacity", 1)
                    .style("stroke", default_highlight_color);
                text
                    .style("opacity", 1)
                    .style("font-weight", "normal")
                    .text(function (d) {
                        //return d.title; 
                        return '';
                    });
                link
                    .style("opacity", 1)
                    .style("stroke", default_link_color);
                arrow
                    .style("fill", default_arrow_color)
                    .style("stroke", default_arrow_color)
                    .style("opacity", default_arrow_opacity);
            }

            zoom.on("zoom", function () {
                var stroke = link_stroke;
                if (link_stroke * zoom.scale() > max_link_stroke) stroke = max_link_stroke / zoom.scale();
                link.style("stroke-width", stroke);
                circle.style("stroke-width", stroke)
                    .attr("r", function (d) {
                        return d.size;
                    });

                if (!text_center) text.attr("dx", function (d) {
                    return d.size;
                });

                var text_size = nominal_text_size;
                if (nominal_text_size * zoom.scale() > max_text_size) text_size = max_text_size / zoom.scale();
                text.style("font-size", text_size + "px");

                g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            });

            svg.call(zoom);

            resize();
        //window.focus();
            d3.select(window).on("resize", resize);

            force.on("tick", tick);

            function countArrowPosition(d) {
                var dx = d.target.x - d.source.x,
                    dy = d.target.y - d.source.y,
                    h = Math.sqrt(dx * dx + dy * dy),
                    r = d.target.size,
                    x = d.target.x - r * dx / h,
                    y = d.target.y - r * dy / h;
                return {x: x, y: y};
            }

            function tick() {
                node.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });
                text.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

                link.attr("x1", function (d) {
                    return d.source.x;
                })
                    .attr("y1", function (d) {
                        return d.source.y;
                    })
                    .attr("x2", function (d) {
                        return countArrowPosition(d).x;
                    })
                    .attr("y2", function (d) {
                        return countArrowPosition(d).y;
                    });

                arrow.attr("d", function (d) {
                    var dx = d.target.x - d.source.x,
                        dy = d.target.y - d.source.y,
                        cos = Math.cos(Math.atan2(dy, dx)),
                        sin = Math.sin(Math.atan2(dy, dx)),
                        x = countArrowPosition(d).x - 2 * cos,
                        y = countArrowPosition(d).y - 2 * sin,
                        length = 10, width = 5;
                    return "M" + x + "," + y + "l" +
                        (width * sin - length * cos) + "," +
                        (-width * cos - length * sin) + "L" +
                        (x - width * sin - length * cos) + "," +
                        (y + width * cos - length * sin) + "z";
                });

                node.attr("cx", function (d) {
                    return d.x;
                })
                    .attr("cy", function (d) {
                        return d.y;
                    });
            }

            function resize() {
                svg.attr("width", width).attr("height", height);
                force.size([force.size()[0] + (width - currentWidth) / zoom.scale(), force.size()[1] + (height - currentHeight) / zoom.scale()]).resume();
                currentWidth = width;
                currentHeight = height;
            }
            });
       }
   };
});       


app.directive('citationNetworkRelevant', function($window,$http){
   return{
      restrict:'EA',
      template:"<svg width='850' height='200'></svg>",
       link: function(scope, elem, attrs){
          var id = scope.pubIdCitNetwork;
          var type = scope.typeCitNetwork;
                //$http.post('/rrs_citations/web/app_dev.php/api/v1/publications', dataObj).
        $http.get('/rrs_citations/web/app_dev.php/api/v1/publications/'+id+'/graphs/'+type).
            success(function (data) {
                var links = data.graphData.graph.links;
                var nodes = data.graphData.graph.nodes;
                console.log();
                citationNetworkShow(elem, links, nodes, $window);
                
            });
       }
   };
});       



app.directive('citationNetworkVenue', function($window,$http){
   return{
      restrict:'EA',
      template:"<svg width='850' height='200'></svg>",
       link: function(scope, elem, attrs){
                //$http.post('/rrs_citations/web/app_dev.php/api/v1/publications', dataObj).
        $http.get('/rrs_citations/web/app_dev.php/api/v1/publications/ACL/graph/venue').
            success(function (data) {
                var links = data.graphData.graph.links;
                var nodes = data.graphData.graph.nodes;
                citationNetworkShow(elem, links, nodes, $window);
                
            });
       }
   };
});       

function citationNetworkShow(elem, links, nodes, $window){

        console.log('links count');
        console.log(links.length);
        console.log('nodes count');
        console.log(nodes.length);
        var width = window.innerWidth;
        var height = window.innerHeight;
        var currentWidth = width;
        var currentHeight = height;
        var link_distance = 150;
        var charge = -50;
        var default_link_color = "#ccc";
        var default_arrow_color = "#888";
        var default_highlight_color = "#888";
        var default_arrow_opacity = 1;
        var highlight_color = "orange";
        var highlight_opacity = 0;
        var nominal_text_size = 10;
        var max_text_size = 24;
        var link_stroke = 1;
        var max_link_stroke = 4.5;
        var min_zoom = 0.1;
        var max_zoom = 7;
        var text_center = false;
        var d3 = $window.d3;
        var force = d3.layout.force()
            .linkDistance(link_distance)
            .charge(charge)
            .gravity(0)
            .size([currentWidth, currentHeight]);
        
        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .html(function (d) {
                return "<strong>Nazev:</strong> <span style='color:orange'>" + d.title + "</span>" +
                    "<br><strong>Autori:</strong> <span style='color:orange'>" + d.authors + "</span>"+
                    "<br><strong>ID:</strong> <span style='color:orange'>" + d.id + "</span>"+
                    "<br><strong>Kategorie:</strong> <span style='color:orange'>" + d.venue_abbr + "</span>";
            })
            .offset([-10, 0]);
    
           
           var rawSvg=elem.find('svg');
           var svg = d3.select(rawSvg[0]).call(tip);
            console.log(tip);
                var graph = { 
                  "links": links ,
                  "nodes": nodes
                }

            //d3.select("body").select("svg").remove();
            //var svg = d3.select("body").append("svg").call(tip);
            var zoom = d3.behavior.zoom().scaleExtent([min_zoom, max_zoom]);
            var g = svg.append("g");
            svg.style("cursor", "move");
                console.log(graph.links);
                console.log(graph.nodes);
            var linkedByIndex = {};
            graph.links.forEach(function (d) {
                linkedByIndex[d.source + "," + d.target] = true;
            });

            function isConnected(a, b) {
                return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index] || a.index == b.index;
            }

            force.nodes(graph.nodes)
                .links(graph.links)
                .start();

            var link = g.selectAll(".link")
                .data(graph.links)
                .enter().append("line")
                .attr("class", "link")
                .style("stroke-width", link_stroke)
                .style("stroke", default_link_color);

            var arrow = g.selectAll("path")
                .data(graph.links)
                .enter().append("svg:path")
                .attr("class", "link")
                .style("fill", default_arrow_color)
                .style("stroke-width", link_stroke)
                .style("stroke", default_arrow_color)
                .style("opacity", default_arrow_opacity);

            var node = g.selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .call(force.drag);

            var circle = node.append("circle")
                .attr("r", function (d) {
                    return d.size;
                })
                .style("fill", function (d) {
                    return d.color;
                })
                .style("stroke-width", link_stroke)
                .style("stroke", default_highlight_color)
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);

            var text = g.selectAll(".text")
                .data(graph.nodes)
                .enter().append("text")
                .attr("dy", ".35em")
                .style("font-size", nominal_text_size + "px");

            if (text_center) {
                text.text(function (d) {
                        if(d.cpi > 0){
                            return d.cpi;
                        }                    
                    return '';
                    return d.title;
                })
                    .style("text-anchor", "middle");
            } else {
                text.attr("dx", function (d) {
                    return d.size;
                })
                    .text(function (d) {
                        if(d.cpi > 0){
                            return d.cpi;
                        }                        
                        return '';
                        return '\u2002' + d.title;
                    });
            }

            var isNodeSelected = false, isNodeHighlighted = false;

            node.on("mouseover", mouseover)
                .on("mousedown", mousedown)
                .on("mouseout", mouseout);

            d3.select(window).on("mouseup", mouseup);

            function mouseover(d) {
                isNodeHighlighted = true;
                if (!isNodeSelected && isNodeHighlighted) {
                    highlightNodeWithRelations(d);
                }
            }

            function mouseout() {
                isNodeHighlighted = false;
                // d3.event.stopPropagation();
                if (!isNodeSelected) {
                    setDefaultStyles();
                }
            }

            function mousedown(d) {
        //            force.stop();
                if (isNodeSelected) {
                    setDefaultStyles();
                    isNodeSelected = false;
                } else {
                    highlightNodeWithRelations(d);
                    isNodeSelected = true;
                }
            }

            function mouseup() {
                force.stop();
            }

            function highlightNodeWithRelations(d) {
                circle
                    .style("opacity", function (o) {
                        return isConnected(d, o) ? 1 : highlight_opacity;
                    })
                    .style("stroke", function (o) {
                        return isConnected(d, o) ? highlight_color : default_highlight_color;
                    });

                text.style("opacity", function (o) {
                    return isConnected(d, o) ? 1 : highlight_opacity;
                }).html(function (o) {
                        if(d.cpi > 0){
                            return d.cpi;
                        }                    
                    return '';
                    return o.title;
                }).style("font-weight", function (o) {
                    return isConnected(d, o) ? "bold" : "normal";
                });

                var setHighlightColor = function (o) {
                    return o.source.index == d.index || o.target.index == d.index ? highlight_color : default_highlight_color;
                };

                link.style("opacity", function (o) {
                    return o.source.index == d.index || o.target.index == d.index ? 1 : highlight_opacity;
                }).style("stroke", setHighlightColor);

                arrow.style("stroke", setHighlightColor)
                    .style("fill", setHighlightColor)
                    .style("opacity", function (o) {
                        return o.source.index == d.index || o.target.index == d.index ? 1 : highlight_opacity;
                    });
            }

            function setDefaultStyles() {
                circle
                    .style("opacity", 1)
                    .style("stroke", default_highlight_color);
                text
                    .style("opacity", 1)
                    .style("font-weight", "normal")
                    .text(function (d) {
                        if(d.cpi > 0){
                            return d.cpi;
                        }
                        return '';
                        return d.title;
                    });
                link
                    .style("opacity", 1)
                    .style("stroke", default_link_color);
                arrow
                    .style("fill", default_arrow_color)
                    .style("stroke", default_arrow_color)
                    .style("opacity", default_arrow_opacity);
            }

            zoom.on("zoom", function () {
                var stroke = link_stroke;
                if (link_stroke * zoom.scale() > max_link_stroke) stroke = max_link_stroke / zoom.scale();
                link.style("stroke-width", stroke);
                circle.style("stroke-width", stroke)
                    .attr("r", function (d) {
                        return d.size;
                    });

                if (!text_center) text.attr("dx", function (d) {
                    return d.size;
                });

                var text_size = nominal_text_size;
                if (nominal_text_size * zoom.scale() > max_text_size) text_size = max_text_size / zoom.scale();
                text.style("font-size", text_size + "px");

                g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            });

            svg.call(zoom);

            resize();
        //window.focus();
            d3.select(window).on("resize", resize);

            force.on("tick", tick);

            function countArrowPosition(d) {
                var dx = d.target.x - d.source.x,
                    dy = d.target.y - d.source.y,
                    h = Math.sqrt(dx * dx + dy * dy),
                    r = d.target.size,
                    x = d.target.x - r * dx / h,
                    y = d.target.y - r * dy / h;
                return {x: x, y: y};
            }

            function tick() {
                node.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });
                text.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

                link.attr("x1", function (d) {
                    return d.source.x;
                })
                    .attr("y1", function (d) {
                        return d.source.y;
                    })
                    .attr("x2", function (d) {
                        return countArrowPosition(d).x;
                    })
                    .attr("y2", function (d) {
                        return countArrowPosition(d).y;
                    });

                arrow.attr("d", function (d) {
                    var dx = d.target.x - d.source.x,
                        dy = d.target.y - d.source.y,
                        cos = Math.cos(Math.atan2(dy, dx)),
                        sin = Math.sin(Math.atan2(dy, dx)),
                        x = countArrowPosition(d).x - 2 * cos,
                        y = countArrowPosition(d).y - 2 * sin,
                        length = 10, width = 5;
                    return "M" + x + "," + y + "l" +
                        (width * sin - length * cos) + "," +
                        (-width * cos - length * sin) + "L" +
                        (x - width * sin - length * cos) + "," +
                        (y + width * cos - length * sin) + "z";
                });

                node.attr("cx", function (d) {
                    return d.x;
                })
                    .attr("cy", function (d) {
                        return d.y;
                    });
            }

            function resize() {
                svg.attr("width", width).attr("height", height);
                force.size([force.size()[0] + (width - currentWidth) / zoom.scale(), force.size()[1] + (height - currentHeight) / zoom.scale()]).resume();
                currentWidth = width;
                currentHeight = height;
            }    
    
}